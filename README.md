# monitoring

This project documents many aspects of the monitoring of services run
by Glasklar.

If you find issues with any of the monitoring services you're welcome
to file an issue [here](https://git.glasklar.is/glasklar/services/monitoring/-/issues).
