# Sigsum use of checker

## Installing checker for Sigsum, manually

Here's how we set up the Sigsum checker installation on tee.sigsum.org.

    root@tee:~# apt install -y python3 python3-twisted python3-future
    root@tee:~# useradd -r -mb /var -s /usr/bin/bash sigsumchecker
    root@tee:~# sudo -iu sigsumchecker
    sigsumchecker@tee:~$ mkdir -p usr/src var/lib var/log
    sigsumchecker@tee:~$ git clone -C usr/src -b sigsum https://git.glasklar.is/sigsum/admin/checker
    sigsumchecker@tee:~$ $EDITOR usr/src/checker/settings.cfg
    sigsumchecker@tee:~$ crontab <<EOF
    @reboot               $HOME/usr/src/checker/main.py -m daemon &
    1-59 *          * * * $HOME/usr/src/checker/main.py -m cron -c minute   >/dev/null 2>&1
    0    1-11,13-23 * * * $HOME/usr/src/checker/main.py -m cron -c hour     >/dev/null 2>&1
    0 0             * * * $HOME/usr/src/checker/main.py -m cron -c day      >/dev/null 2>&1
    0 12            * * * $HOME/usr/src/checker/main.py -m cron -c day_noon >/dev/null 2>&1
    EOF

We're monitoring the Sigsum logs and a few web services using this
instance. Email alerts are being sent to a
(mailing list)[https://lists.sigsum.org/mailman3/postorius/lists/sigsum-log-monitor.lists.sigsum.org/].

Our checker instance has one checker peer run by rgdd. This means that
if the host it runs on, tee, becomes unable to deliver email to the
list server (madvise), our peer will alert us (over email).

### IMAP and SMTP password

The password for authenticating with mail.glasklarteknik.se is being
stored unencrypted in settings.cfg, kept locally on tee. To limit the
damage done when it's exposed we create an "app password" in mailcow
and limit it to SMTP and IMAP.
