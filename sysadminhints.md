## External checks

We are using [checker](https://git.glasklar.is/sigsum/admin/checker)
for monitoring some of our services.

See [Sigsum use of checker](checker.md) for details on our checker installation.


## Local systems monitoring

We are using [netdata](https://learn.netdata.cloud/) for monitoring
system health locally.

We don't expose the web GUI on the internet. You can set up an SSH
tunnel to port 19999 and point a browser at it. Example:

	ssh -NfL 4711:localhost:19999 logsrv-01.sigsum.org
	firefox http://localhost:4711/

Current config can be found at /netdata.conf:

    curl localhost:4711/netdata.conf


### Configuring netdata

Netdata is configured by first editing a file using the `edit-config`
script and then restarting the netdata daemon

    systemctl restart netdata

General netdata config:

    sudo /etc/netdata/edit-config netdata.conf

Configuring netdata to be sending email or not, set SEND_EMAIL to YES or NO (this is a shell script):

    sudo /etc/netdata/edit-config health_alarm_notify.conf

Testing the sending of email alerts:

    sudo -u netdata env NETDATA_ALARM_NOTIFY_DEBUG=1 /usr/lib/netdata/plugins.d/alarm-notify.sh test

Configuring disk alarms, like disk.backlog, can be done by editing
health.d/disks.conf. Note that this affects all disk volumes.

    server$ sudo /etc/netdata/edit-config health.d/disks.conf
